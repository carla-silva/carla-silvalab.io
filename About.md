# About

> Career | Researcher :: PhD in Computer Science | e-mail :: carla.maps@gmail.com

## Brief

**Carla Silva** graduated (2007) from the Faculty of Sciences, University of Porto, Portugal. She received the PhD degree (2021) in **Computer Science** on the topic Quantum Artificial Intelligence (QAI) from the Faculty of Sciences, University of Porto, Portugal. In the past, she was principal researcher, postdoc fellow, research fellow and developer. She has participated in projects both in industry and research institutions. Her main work activities are as **scientific programmer**, **data scientist** and **researcher**. Her research interests include many aspects of computer science, namely, artificial intelligence, data mining, and the development of algorithms for multiple applications through a data-driven strategy to derive knowledge from data.

## Web

* [Web of Science](https://www.webofscience.com/wos/author/record/83746)
* [Scopus](https://www.scopus.com/authid/detail.uri?authorId=56263913400)
* [CienciaVitae](https://www.cienciavitae.pt/en/5D1C-C504-DE39)
* [ORCID](https://orcid.org/0000-0002-4941-1009)
* [Researchgate](https://www.researchgate.net/profile/Carla_Silva63)
* [Authenticus](https://www.authenticus.pt/pt/profileOfResearchers/publicationsList/555699)
* [Google Scholar](https://scholar.google.pt/citations?user=qzLi-IwAAAAJ&hl)
* [DBLP](https://dblp.org/pid/158/2351-2)
* [Kudos](https://www.growkudos.com/profile/carla_silva_1)
* [Portfolio](https://cmaps.portfoliobox.net)


