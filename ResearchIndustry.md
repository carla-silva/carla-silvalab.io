# Research & Industry

## - Research

> #### Principal Researcher
> [INEGI – Institute of Science and Innovation in Mechanical and Industrial Engineering, Porto, Portugal](https://www.inegi.pt/en/)

* **Project** | Development and Implementation of Algorithms for Data Processing from Production and Automation Systems. (*2022/08 - 2022/11*)
* **Description** | Research activities (e.g. writing proposals), data science (e.g. NLP - Web scraping - Web mining) and AI algorithms development for automation and to extract knowledge from industrial processes to improve e.g. efficiency and resources from advanced manufacturing systems.
* **Technologies** | Python, Jupyter notebooks.

> #### Post-doctoral Fellow
> [SYSTEC - Research Center For Systems and Technologies | ISR-Porto - Institute of Systems and Robotic, Faculty of Engineering, University of Porto](https://systec.fe.up.pt/)

* **Project** | MLDLCOV. (*2022/03 - 2022/07*)
* **Description** | Impact of COVID-19 containment measures on mobility, air pollution, health and macroeconomic indicators in Portugal: a Machine Learning approach. Efforts to build a deep learning model, namely, the Auxiliary Classifier GAN, or AC-GAN for short, which is an extension of the conditional GAN that changes the discriminator to predict the class label, in the implemented script e.g. predict the air pollution.
* **Technologies** | MATLAB, Python, Google Colaboratory (Colab) / Jupyter notebooks.

> #### Research Fellow
> [Faculty of Sciences, University of Porto, Portugal](https://sigarra.up.pt/fcup)

* **Project** | Safe Cities. (*2020/03 - 2021/08*)
* **Description** | Data science and machine learning tasks (e.g. exploratory data analysis, modeling and forecast) for predictive maintenance. Namely, in the context of handling almost big data, the AutoRegressive Integrated Moving Average (ARIMA) and Holt-Winters (HW) modelling. In addition, extract knowledge of sensors behavior resorting to probabilistic graphical
models (PGMs): Bayesian networks (BNs) and dynamic Bayesian networks (DBNs), a time-based BN version. These graphical models can be very useful to get insights from data to improve sensor capabilities in the industry of fire detection systems.
* **Technologies** | Python, Dash - Plotly, Jupyter notebooks, R programming language.

> #### Research Fellow
> [IT - Telecommunications Institute, Faculty of Engineering, University of Porto, Portugal](http://www.it.pt)

* **Project** | S2MovingCity. New knowledge and a proof of concept for massive urban sensing of vehicles, and environment, supporting data-driven city planning and decision making. (*2019/01 - 2019/08*)
* **Description** | Analysis, processing and cleaning of urban data sets of such as mobility, weather, among others. Development and / or enhancement of a data mining pipeline, including visualization module (interactive) and performance evaluation. Application of graph techniques. Improve, combine and / or develop automatic learning algorithms to understand and model urban phenomena.
* **Technologies** | R Shiny / R programming language.

> #### Research Fellow
> [IT - Telecommunications Institute, Faculty of Engineering, University of Porto, Portugal](http://www.it.pt)

* **Project** | SmartCityMules. Mobile data collection and dissemination. (*2017/11 - 2018/12*)
* **Description** | Traffic flow spatial-temporal reasoning, data mining and machine learning. The research explains the complex associations of traffic flow based in an empirical-theoretical framework using real-world datasets. We propose methods to infer microscopic fundamental diagrams in dense urban areas making use of inductive loop detectors and taxi trajectory data. Identify and quantify causalities between congestion and diverse confounding variables (e.g. meteorological conditions).
* **Technologies** | Python and R programming language.

> #### Research Fellow
> [CINTESIS - Center for Research in Health Technologies and Services, Faculty of Medicine, University of Porto, Portugal](http://www.cintesis.eu)

* **Project** | NanoSTIMA. Bayesian networks for exploring multimorbidity and diseases. (*2016/08 - 2017/03*)
* **Description** | Data science, temporal scenarios from hospital electronic health records. Explain the complexity of multimorbidity (two or more diseases simultaneously) in patients with acute myocardial infarction through time-based Bayesian network abstractions. Univariate and multivariate analyses were conducted through dynamic and temporal Bayesian networks, considering patient monitoring over a period of time (5 follow-up). The structure of a network intends to graphically summarize Bayesian metaphors for multimorbidity.
* **Technologies** | R programming language.

> #### Research Fellow
> [CRACS INESC TEC - Center for Research in Advanced Computing Systems, Faculty of Sciences, University of Porto, Portugal](http://cracs.fc.up.pt)

* **Project** | INCENTIVO PESt (CRACS). Implementation of specific data science functions from scratch. (*2014/06 - 2015/12*)
* **Description** | Databases schemas, statistics, data mining, programming in diverse languages. Statistical functions, generally applied to data analysis, and assess their performance as implemented by MATLAB, R and the software Dataip. The use of medium to large datasets to show that Dataip outperforms MATLAB and R by several orders of magnitude. Speedup and performance measurements.
* **Technologies** | PostgreSQL, C, R and MATLAB programming languages.

## - Industry

> #### Data Scientist
> [Bosch Security Systems, Inc.](https://www.bosch.pt)

* **Project** | Fire detection industry, sensor data. Use cases development on the topics: energy power supply, false alarm mitigation and predictive maintenance. (*2021/09 - 2022/03*)
* **Description** | Development of multiple web applications / dashboards using Python Dash with Machine Learning and Deep Learning models (e.g. Convolutional Neural Network (ConvNet/CNN), Long short-term memory (LSTM)).
* **Technologies** | Databases - MongoDB, Python, Dash - Plotly, R programming language.

> #### Game Math Software Engineer
> [FABAMAQ - Sistemas Informáticos, SA](http://www.fabamaq.com)

* **Project** | Game industry, e.g. slot machines. Store and organize unstructured data from game machine logs placed in different countries worldwide. (*2017/03 - 2017/09*)
* **Description** | Data wrangling, data visualization and data analytics. As part of the Game Math team, focusing on setting new rules for the payouts of each game and perform data analysis to identify possible optimisations to apply to current and future games. Exploratory data analysis and prediction with machine learning algorithms. Development of a web application using R Shiny and parallel programming.
* **Technologies** | Databases and R programming language.
